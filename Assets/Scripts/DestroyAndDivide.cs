using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAndDivide : MonoBehaviour
{

    const float TimeRemainingBeforeDeath = 3;

    [SerializeField]
    private GameObject toSpawn;
    private GameObject myCreatorHasCollidedWith;

    public float timeRemainingBeforeDeath = TimeRemainingBeforeDeath;


    public GameObject MyCreatorHasCollidedWith { get => myCreatorHasCollidedWith; set => myCreatorHasCollidedWith = value; }

    // Start is called before the first frame update
    private void Start()
    {
        Destroy(gameObject, 15);
    }

    private void FixedUpdate()
    {
        if (GetComponent<Rigidbody>().velocity == Vector3.zero)
        {
            timeRemainingBeforeDeath -= Time.deltaTime;
            if (timeRemainingBeforeDeath <= 0)
                Destroy(gameObject);
        }
        else
            timeRemainingBeforeDeath = TimeRemainingBeforeDeath;
    }

    private void OnCollisionEnter(Collision collision)
    {
        GameObject hasCollided = collision.gameObject;
        Transform colliderParent = hasCollided.transform.parent;
        print("ciao=" + hasCollided + " no" + colliderParent + " " + myCreatorHasCollidedWith);
        if (
            !hasCollided.CompareTag("Pavimento")
            && this.transform.parent!=colliderParent
            && !GameObject.ReferenceEquals(hasCollided, myCreatorHasCollidedWith)
            )
        {
            if (colliderParent == null || !GameObject.ReferenceEquals(colliderParent.gameObject, myCreatorHasCollidedWith))
            {
                if (toSpawn != null)
                {
                    GameObject parent = new GameObject();
                    parent = Instantiate(parent, transform.parent, hasCollided);
                    createClone(2, parent, hasCollided);
                }
                Destroy(gameObject);
            }
        }
    }

    private void createClone(int v, GameObject parent, GameObject creatorHasCollidedWith)
    {
        for (int i = 0; i < v; i++)
        {
            GameObject clone;
            clone = Instantiate(toSpawn, transform.position, transform.rotation);
            clone.transform.position = UnityEngine.Random.onUnitSphere * transform.localScale.x + transform.position;
            clone.GetComponent<Rigidbody>().velocity = this.GetComponent<Rigidbody>().velocity;
            clone.GetComponent<Rigidbody>().angularVelocity = this.GetComponent<Rigidbody>().angularVelocity;
            DestroyAndDivide script = clone.GetComponent<DestroyAndDivide>();
            if (script != null)
            {
                script.MyCreatorHasCollidedWith = creatorHasCollidedWith;
            }
            clone.transform.parent = parent.transform;
        }
    }
}
