using UnityEngine;
using UnityEngine.UI;

public class ChangeSliderText : MonoBehaviour
{

    [SerializeField]
    private string labelTextSliderPower;

    private Text text;

    void Start()
    {
        text = GetComponent<Text>();
    }

    public void updateText(float power)
    {
        text.text = labelTextSliderPower + " " + power;
    }

}
