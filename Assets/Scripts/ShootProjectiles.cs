using System.Collections.Generic;
using UnityEngine;

public class ShootProjectiles : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> possiblesProjectiles;

    private float fireFrequency = 0.2f;
    private bool shootingPaused = false;
    private float power = 1;
    public float Power
    {
        set
        {
            if (value >= 0.5 || value <= 2)
            {
                power = value;
                CancelInvoke();
                InvokeRepeating("LaunchProjectile", 0.5f, fireFrequency/power);
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("LaunchProjectile", 0.5f, fireFrequency);
    }


    void LaunchProjectile()
    {
        if (!shootingPaused)
        {
            GameObject instance = possiblesProjectiles[Random.Range(0, possiblesProjectiles.Count)];
            GameObject empty = new GameObject();

            empty = Instantiate(empty, transform.position, transform.rotation);
            instance = Instantiate(instance, transform.position, transform.rotation);
            instance.transform.SetParent(empty.transform);

            instance.GetComponent<Rigidbody>().velocity = Random.insideUnitSphere * 20;
        }
    }


    public void pauseOrResumeShooting()
    {
        shootingPaused = !shootingPaused;
    }

}
