using UnityEngine;
using UnityEngine.UI;


public class ChangeButtonText : MonoBehaviour
{

    [SerializeField]
    private string activeCannonMessage;
    [SerializeField]
    private string disabledCannonMessage;

    private Text text;

    //i cannoni partono abilitati
    private bool cannonDisabled = false;
   

    private void Start()
    {
        text = GetComponent<Text>();
    }

    public void changeText()
    {
        cannonDisabled = !cannonDisabled;
        text.text = (cannonDisabled) ? disabledCannonMessage : activeCannonMessage;
    }
}
